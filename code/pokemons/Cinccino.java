package pokemons;
import attacks.FocusBlast;
import attacks.Rest;
import attacks.TailSlap;
import attacks.Tickle;
import ru.ifmo.se.pokemon.Type;

public class Cinccino extends Minccino{
    public Cinccino(String name, int level){
        super(name, level);
        setStats(75,95,60,65,60,115);
        setType(Type.NORMAL);
        setMove(new TailSlap(), new Tickle(), new Rest(), new FocusBlast());
    }
}
