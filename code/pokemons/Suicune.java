package pokemons;
import ru.ifmo.se.pokemon.*;
import attacks.*;

public class Suicune extends Pokemon{
    public Suicune (String name, int level){
        super(name, level);
        setStats(100, 75, 115, 90, 115, 85);
        setType(Type.WATER);
        setMove(new ShadowBall(), new Facade(), new Bite(), new IceBeam());
    }
}