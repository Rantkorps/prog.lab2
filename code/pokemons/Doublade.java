package pokemons;
import attacks.Confide;
import attacks.Facade;
import attacks.Tackle;
import ru.ifmo.se.pokemon.Type;

public class Doublade extends Honedge{
    public Doublade(String name, int level){
        super(name, level);
        setStats(59,110,150,45,49,35);
        setType(Type.STEEL, Type.GHOST);
        setMove(new Confide(), new Facade(), new Tackle());
    }
}
