package pokemons;
import attacks.Rest;
import attacks.TailSlap;
import attacks.Tickle;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.Type;

public class Minccino extends Pokemon {
    public Minccino(String name, int level){
        super(name, level);
        setStats(55,50,40,40,40,75);
        setType(Type.NORMAL);
        setMove(new TailSlap(), new Tickle(), new Rest());
    }
}
