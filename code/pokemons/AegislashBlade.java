package pokemons;
import attacks.Confide;
import attacks.Facade;
import attacks.Rest;
import attacks.Tackle;
import ru.ifmo.se.pokemon.Type;

public class AegislashBlade extends Doublade{
    public AegislashBlade (String name, int level){
        super(name, level);
        setStats(60,50,140,50,140,60);
        setType(Type.STEEL, Type.GHOST);
        setMove(new Confide(), new Facade(), new Tackle(), new Rest());
    }
}
