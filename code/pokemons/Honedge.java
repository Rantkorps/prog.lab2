package pokemons;
import attacks.Confide;
import attacks.Facade;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.Type;

public class Honedge extends Pokemon {
    public Honedge (String name, int level){
        super(name, level);
        setType(Type.STEEL, Type.GHOST);
        setStats(45,80,100,35,37,28);
        setMove(new Confide(), new Facade());
    }
}
