import ru.ifmo.se.pokemon.*;
import pokemons.*;


public class Main {
    public static void main(String[] args) {
        Battle battle = new Battle();
        battle.addAlly(new Suicune("Кот", 50));
        battle.addAlly(new AegislashBlade("Супер меч", 50));
        battle.addAlly(new Honedge("Меч", 50));
        battle.addAlly(new Doublade("Дабл меч", 50));
        battle.addFoe(new Minccino("Крыса", 50));
        battle.addFoe(new Cinccino("Мышь", 50));
        battle.go();
    }
}
