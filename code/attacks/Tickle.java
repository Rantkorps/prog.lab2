package attacks;
import ru.ifmo.se.pokemon.*;

public class Tickle extends StatusMove {
    public Tickle() { super(Type.NORMAL, 0, 100 ); }

    public String describe() { return "снижает атаку и защиту, используя Tickle"; }

    @Override
    public void applyOppEffects(Pokemon p) {
        p.addEffect( new Effect().stat(Stat.ATTACK, 1));
        p.addEffect( new Effect().stat(Stat.DEFENSE, 1));
    }
}
