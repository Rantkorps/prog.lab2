package attacks;
import ru.ifmo.se.pokemon.*;

public class Facade extends PhysicalMove {
    public Facade() { super(Type.NORMAL, 70, 100); }

    @Override
    protected String describe() { return "наносит урон, используя Facade"; }

    @Override
    protected void applyOppEffects(Pokemon p) {
        this.power = 70;
        if (p.getCondition() == Status.BURN || p.getCondition() == Status.POISON || p.getCondition() == Status.PARALYZE)
            this.power *=2;
    }
}