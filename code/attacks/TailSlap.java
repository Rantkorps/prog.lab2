package attacks;
import ru.ifmo.se.pokemon.PhysicalMove;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.Type;

public class TailSlap extends PhysicalMove {
    public TailSlap() { super(Type.NORMAL, 25,85, 4, 5);}

    public String describe() { return "наносит урон, используя TailSlap"; }

    @Override
    public void applyOppEffects(Pokemon p) {
        if (Math.random() < 0.375){
            this.power = 50;
            this.hits = 2;
            if (Math.random() < 0.375){
                this.power = 75;
                this.hits = 3;
                if (Math.random() < 0.125){
                    this.power = 100;
                    this.hits =4;
                    if (Math.random() < 0.125) {
                        this.power = 125;
                        this.hits = 5;
                    }
                }
            }
        }
    }
}
