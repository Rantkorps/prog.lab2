package attacks;
import ru.ifmo.se.pokemon.*;

public class Rest extends StatusMove {
    public Rest() { super(Type.PSYCHIC, 0, 0); }

    public String describe() { return "спит и полностью исцеляется, используя Rest"; }

    @Override
    public void applySelfEffects(Pokemon p) {
        Effect.sleep(p);
        p.restore();
    }
}