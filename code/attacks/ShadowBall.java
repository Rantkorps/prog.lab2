package attacks;
import ru.ifmo.se.pokemon.*;

public class ShadowBall extends SpecialMove {
    public ShadowBall() { super(Type.GHOST, 80, 100); }

    @Override
    protected String describe()  {
        return "наносит урон, используя ShadowBall";
    }

    @Override
    protected void applyOppEffects(Pokemon p) { p.addEffect( new Effect().chance(0.2).stat(Stat.SPECIAL_DEFENSE, -1)); }
}
