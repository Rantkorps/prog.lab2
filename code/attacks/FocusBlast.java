package attacks;
import ru.ifmo.se.pokemon.*;

public class FocusBlast extends SpecialMove {
    public FocusBlast() { super(Type.FIGHTING, 120, 70); }

    public String describe() { return "наносит урон, используя FocusBlast"; }

    @Override
    public void applyOppEffects(Pokemon p) { p.addEffect( new Effect().chance(0.1).stat(Stat.SPECIAL_DEFENSE, 1));}
}
