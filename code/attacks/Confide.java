package attacks;
import ru.ifmo.se.pokemon.*;

public class Confide extends StatusMove {
    public Confide() { super(Type.NORMAL, 0, 0); }

    public String describe() { return "снижает специальную атаку, используя Confide"; }

    @Override
    public void applyOppEffects(Pokemon p) { p.addEffect( new Effect().stat(Stat.SPECIAL_DEFENSE, 1)); }
}
